#!/usr/bin/python
import subprocess, shutil, tarfile, os, logging
import json, urllib2, requests

class NZBget_updator(object):

    def __init__(self):
        logging.basicConfig(filename='nzbget-updater.log',level=logging.INFO)
        if os.getuid() != 0:
            logging.error('Please run me elevated.')
            return 0
        try:
            # Add a try except to keep going if the file doesn't exists (first time run)
            logging.info('INFO: Reading current.json file')
            self.current_json = json.loads(open('current.json').read())
        except IOError:
            # If the current file doesn't exists set it to 0
            logging.warning('WARN: File current.json does not exists: continuing to download all required files')
            self.current_json = 0
        finally:
            self.remote_user = "zbosh"
            self.remote_host = "ftp.zbosh.com"
            self.remote_port = "22"

            # Get the latest version of nzbget
            logging.info('INFO: Retreiving the latest versions')
            self.latest_json = json.loads(requests.get('http://nzbget.net/info/nzbget-version.php').text.split(' = ')[1])
            self.check_for_updates()

    def check_for_updates(self):
        if self.current_json != 0:
            # If the current version dictionary is not the same as the latest version
            logging.info('INFO: Checking json current with latest')
            if self.current_json != self.latest_json:
                try:
                    self.check_version('stable-version')
                    self.check_version('testing-version')
                    self.write_json()
                except:
                    logging.warning('ERROR: Something bad happened')
                    return -1
            else:
                logging.info('INFO: No new versions found')
                return 0
        else:
            self.download_version('stable-version')
            self.compile_new_version('stable-version')
            self.download_version('testing-version')
            self.compile_new_version('testing-version')
            self.write_json()
            return 0

        logging.info('INFO: End of check for updates method')

    def write_json(self):
        try:
            #Save current.json to file
            with open('current.json','w') as outfile:
                json.dump(self.latest_json, outfile)
        except:
            logging.error('ERROR: Could not write the json file')
            return -1

    def untar_slackbuild(self):
        tarfile.open('nzbget-SlackBuilds.tar.gz').extractall('.')
        os.chmod('nzbget-SlackBuilds/nzbget.SlackBuild',755)

    def move_downloads(self, version_name):
        try:
            logging.info('INFO: Removing nzbget-%s.tar.gz from the slackbuilds directory', self.latest_json[version_name])
            os.remove('./nzbget-SlackBuilds/nzbget-' + self.latest_json[version_name] + '.tar.gz')
        except:
            logging.warn('WARN: Failed to remove nzbget-%s.tar.gz file does not exist or unable to remove', self.latest_json[version_name])
        finally:
            shutil.move('nzbget-' + self.latest_json[version_name] + '.tar.gz','./nzbget-SlackBuilds/')

    def download_version(self, version_name):
        r = requests.get('http://sourceforge.net/projects/nzbget/files/nzbget-' + self.latest_json[version_name] + '.tar.gz', stream=True)
        with open('nzbget-' + self.latest_json[version_name] + '.tar.gz', 'wb') as f:
            for chunk in r.iter_content(chunk_size=1024):
                if chunk:
                    f.write(chunk)
                    f.flush()
        self.untar_slackbuild()
        self.move_downloads(version_name)

    def check_version(self, version_name):
        logging.info('INFO: Checking current and latest for version %s', version_name)
        if self.current_json[version_name] != self.latest_json[version_name]:
            logging.info('INFO: Downloading the latest version %s from sourceforge',  version_name)
            try:
                self.download_version(version_name)
                self.compile_new_version(version_name)
            except:
                logging.error('ERROR: Downloading or writing file failed')

    def compile_new_version(self, version_name):
        # Run the shell comand below
        logging.info('INFO: Compiling %s', version_name)
        previous_path = os.getcwd()
        os.chdir("nzbget-SlackBuilds")
        logging.info('INFO: Starting the subprocess for compiling')
        proc = subprocess.Popen(['sudo VERSION=' + self.latest_json[version_name] + ' ./nzbget.SlackBuild'], stdin=subprocess.PIPE, stderr=subprocess.PIPE, stdout=subprocess.PIPE, shell=True)
        logging.info('INFO: %s', proc.stdout.read())
        logging.error('ERROR: %s', proc.stderr.read())
        os.chdir(previous_path)
        self.update_web_json(version_name, self.latest_json[version_name])
        self.upload_web_file(self.latest_json[version_name])

    def update_web_json(self, version_name, version_number):
        # ssh -t " + self.remote_user + "@" + self.remote_host + " -p " + self.remote_port "cp ~/nzbget/nzbget-update-info.json ~/nzbget/nzbget-update-info.json.bak && sed -i \"s/fcn1('param1', $2)\n/fcn2('param2'):$zoom\n/g\" ~/nzbget/nzbget-update-info.json"
        # ssh -t zbosh@ftp.zbosh.com "sed -i 's/\"stable-version\": \".*\",/\"stable-version\": \"112\",/g' ~/zbosh.com/nzbget/nzbget-update-info.json"
        logging.info('INFO: Running Sed on server for %s with %s', version_name, version_number)
        logging.info('INFO: Starting the subprocess for running sed')
        proc = subprocess.Popen(['ssh -t ' + self.remote_user + '@' + self.remote_host + ' -p ' + self.remote_port + ' \"sed -i \'s/\\"' + version_name + '\\": \\".*\\",/\\"' + version_name + '\\": \\"' + version_number + '\\",/g\' ~/zbosh.com/nzbget/nzbget-update-info.json\"'], stderr=subprocess.PIPE, stdout=subprocess.PIPE, shell=True)
        logging.info('INFO: %s', proc.stdout.read())
        logging.error('ERROR: %s', proc.stderr.read())

    def upload_web_file(self, version_number):
        file_name = "nzbget-" + version_number + "-x86_64-1_SBo.tgz"
        logging.info('INFO: Uploading %s to server', file_name)
        proc = subprocess.Popen(['scp /tmp/' + file_name + ' ' + self.remote_user + '@' + self.remote_host + ':~/zbosh.com/nzbget/' + file_name], stderr=subprocess.PIPE, stdout=subprocess.PIPE, shell=True)
        logging.info('INFO: scp /tmp/' + file_name + ' ' + self.remote_user + '@' + self.remote_host + ':~/zbosh.com/nzbget/' + file_name)
        logging.info('INFO: %s', proc.stdout.read())
        logging.error('ERROR: %s', proc.stderr.read())

nzb = NZBget_updator()
