#!/bin/sh

echo "Installing ${NZBUP_BRANCH}";
echo "Downloading new version...";
cd /tmp
URL_USED=$(curl "http://zbosh.com/nzbget/download.php?ver=${NZBUP_BRANCH}" -s -L -J -O -w '%{filename_effective}')
if [ -n "$URL_USED" ]; then
  echo "Downloading new version...OK";
else 
  echo "[ERROR] Wget couldn't download the file"
fi

echo "Stopping NZBGet...";
# make a pause after printing the last message, which nzbget can receve from the script
sleep 1;

"${NZBOP_APPBIN}" -c "${NZBOP_CONFIGFILE}" -Q

  if [ -z $NZBUP_PROCESSID ]; then
    #grab pid from pid file
    Pid="${NZBUP_PROCESSID}"
    i=0
    /bin/kill $Pid
    /bin/echo -n " Waiting for nzbget to shut down: "
    while [ -d /proc/$Pid ]; do
       sleep 1
       let i+=1
       /bin/echo -n "$i, "
       if [ $i = 45 ]; then
          /bin/echo -n " Tired of waiting, killing nzbget now"
          /bin/kill -9 $Pid
          /bin/echo " Done ($(/bin/date))"
          exit 1
       fi
    done
    /bin/echo "Done ($(/bin/date))"
  else
    /bin/echo "nzbget is not running? ($(/bin/date))"
  fi
sleep 5;

# upgrade nzbget
cp /etc/nzbget.conf /etc/nzbget.conf.bak
OLD_PKG=$(ls /var/log/packages | grep nzbget)
# removepkg -warn $OLD_PKG

upgradepkg ${OLD_PKG}%${URL_USED}


cp /etc/nzbget.conf.bak /etc/nzbget.conf 
echo "Starting NZBGet...";
"${NZBOP_APPBIN}" -c "${NZBOP_CONFIGFILE}" -D