<?php
  // Get the json file and convert to object
  $json_data = str_replace("NZBGet.UpdateInfo = ", "", file_get_contents('nzbget-update-info.json'));
  $obj = json_decode($json_data);

  //set a variable to store the file name
  $file_name = "";

  // Load the Get and conver it to the version in the json.
  if($_GET["ver"] === "STABLE")
    {
      $file_name="nzbget-" . $obj->{'stable-version'} . "-x86_64-1_SBo.tgz"; // Create the file names.
    }
    else if($_GET["ver"] === "TESTING")
    {
      $file_name="nzbget-" . $obj->{'testing-version'} . "-x86_64-1_SBo.tgz";
    }
    else if($_GET["ver"] === "DEVEL")
    {
      $file_name="nzbget-" . $obj->{'devel-version'} . "-x86_64-1_SBo.tgz";
    }
  // if the file exists return the file
    clearstatcache();
    if(file_exists(dirname(__FILE__) . "/" . $file_name)){
      _Download(dirname(__FILE__) . "/" . $file_name, $file_name);
    }  
    else{
      echo "Files Doesn't Exist";
    }

function _Download($f_location, $f_name){
  header('Content-Description: File Transfer');
  header('Content-Type: application/octet-stream');
  header('Content-Length: ' . filesize($f_location));
  header('Content-Disposition: attachment; filename=' . basename($f_name));
  readfile($f_location);
}

  // $dir = new DirectoryIterator(dirname(__FILE__));
  // foreach ($dir as $fileinfo) {
  //     if (!$fileinfo->isDot()) {
  //         var_dump($fileinfo->getFilename());
  //     }
  // }

?>